package com.wipro.one2many.entity;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "comments")
@Data
@NoArgsConstructor
public class Comment {
	
	public Comment(String text) {
		super();
		this.text = text;
	}
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String text;
	
}
